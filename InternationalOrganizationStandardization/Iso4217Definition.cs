﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InternationalOrganizationStandardization
{
    public class Iso4217Definition
    {
        private string _code;
        private int _number;
        private double _exponent;
        public string Code
        {
            get { return _code; }
        }
        public int Number
        {
            get { return _number; }
        }
        public double Exponent
        {
            get { return _exponent; }
        }
        public Iso4217Definition(string code, int number, double exponent)
        {
            _code = code;
            _number = number;
            _exponent = exponent;
        }
    }
}
